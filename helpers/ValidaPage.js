'use strict';

var ValidaPage = function(){

    //Elemento Toast de sucesso e falha
    this.sucesso = element(by.className('dx-toast-success'));
    this.falha = element(by.className('dx-toast-error'));

    this.validaSucesso = function(){
        browser.ignoreSynchronization = true;
        browser.wait(EC.visibilityOf(this.sucesso),10000,console.log());
        return this.sucesso.getAttribute('class');
    };

};

module.exports = ValidaPage;