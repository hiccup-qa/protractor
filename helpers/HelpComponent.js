'use strict';

var HelpComponent = function(){

    this.setInputAutocomplete = function(elemento, palavra){
        var cont = 0;
        var res = palavra.split("");
        while(cont < palavra.length){
            elemento.sendKeys(res[cont].toString());
            cont = cont + 1;
            browser.sleep(100);
        }
    };

    this.selectItemPopUp = function(elementoInput,stringInput,elementoPopUp){
        this.setInputAutocomplete(elementoInput, stringInput);
        browser.wait(element(by.xpath(elementoPopUp)).isDisplayed, 5000, 'Não foi possivel selecionar o item da caixa de seleção');
        browser.sleep(200);
        element.all(by.xpath(elementoPopUp)).first().click();
    };

    this.selectItemComboBox = function(elementoCombo,elementoPopUp,popUpItem){
        elementoCombo.click();
        browser.wait(element(by.xpath(elementoPopUp)).isDisplayed, 5000, 'Não foi possivel selecionar o item da caixa de seleção');
        element.all(by.xpath(elementoPopUp)).get(popUpItem).click();
    };

    this.geraNum = function(tamanho){
        return Math.floor(Math.random() * tamanho);
    };

    this.geraString = function(palavra){
        return palavra.toString() + ' - ' + Math.floor(Math.random() * 9999);
    };

    this.geraStringRandom = function(tamanho){
        var caracter = 'ABCDEFGHIJKLMNOPQRSTUVXYZW'
        var text = '';
        for(var cont = 0; cont < tamanho; cont++){
            text += caracter.charAt(Math.floor(Math.random() * caracter.length));
        }
        return text;
    };
  
    this.geraDateRandom = function(){
        return Math.floor(Math.random()*12 + 1)+'/'+ Math.floor(Math.random()*2100);     
    };
    
};

module.exports = HelpComponent;