'use strict';

var LoginPage = function(){
    this.usuario = element(by.name('username'));
    this.senha = element(by.name('password'));
    this.btnEntrar = element(by.name('login-form-entrar'));
    this.opManterConectado = element(by.name('manter-conectado'));

    this.logar = function(usuario, senha, manterConectado){
        this.usuario.sendKeys(usuario);
        this.senha.sendKeys(senha);
        if(manterConectado == true){
            this.opManterConectado.click();
        }
        this.btnEntrar.click();
    };


};

module.exports = LoginPage;