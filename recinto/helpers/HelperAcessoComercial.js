'use strict';

var HelperAcessoComercial = function(){

        //Elementos do menus principais
        this.menuPrincipal = element(by.css('[href="#Importação"]'));
        this.menuGestaoCad = element(by.css('[href="#Gestão e Cadastros"]'));
    
        //Elementos do sub-menu principal importação
        this.programacao = element(by.css('[href="#Programação"]'));
        this.cadastros = element(by.css('[href="#Cadastros"]'));
        this.listas = element(by.css('[href="#Listas"]'));
    
        //Elementos itens do sub-menu Importação/Cadastros
        this.cadEtiqueta = element(by.css('[href="/importacao/cadastros/etiqueta"]'));
        this.cadRestricao = element(by.css('[href="/importacao/cadastros/restricao"]'));
        this.cadPorto = element(by.css('[href="/importacao/cadastros/porto"]'));
        this.cadEmbarcacao = element(by.css('[href="/importacao/cadastros/embarcacao"]'));
        this.cadProduto = element(by.css('[href="/importacao/cadastros/categoria"]'));
        this.cadBudget = element(by.css('[href="/importacao/cadastros/budget"]'));
        this.cadProgramacao = element(by.css('[href="/importacao/programacao/list"]'));

        this.acessaCadProgramacao = function(){
            this.menuPrincipal.click();
            this.programacao.click();
            this.cadProgramacao.click();
        };
    
        this.acessaCadEtiqueta = function(){
            this.menuPrincipal.click();
            this.cadastros.click();
            this.cadEtiqueta.click();
        };
    
        this.acessaCadRestricao = function(){
            this.menuPrincipal.click();
            this.cadastros.click();
            this.cadRestricao.click();
        };
    
        this.acessaCadPorto = function(){
            this.menuPrincipal.click();
            this.cadastros.click();
            this.cadPorto.click();
        };
    
        this.acessaCadEmbarcacao = function(){
            this.menuPrincipal.click();
            this.cadastros.click();
            this.cadEmbarcacao.click();
        };
    
        this.acessaCadProduto = function(){
            this.menuPrincipal.click();
            this.cadastros.click();
            this.cadProduto.click();
        };
    
        this.acessaCadBudget = function(){
            this.menuPrincipal.click();
            this.cadastros.click();
            this.cadBudget.click();
        };

}

module.exports = HelperAcessoComercial;