'use strict';

var AcessoPage = function(){

    //Elementos do menus principais
    this.menuPrincipal = element(by.css('[href="#Comercial"]'));
    this.menuGestaoCad = element(by.css('[href="#Gestão e Cadastros"]'));

    //Elementos do sub-menu principal importação
    this.programacao = element(by.css('[href="#Programação"]'));
    this.cadastros = element(by.css('[href="#Cadastros"]'));
    this.listas = element(by.css('[href="#Listas"]'));

    //Elementos itens do sub-menu Importação/Cadastros
    this.cadEtiqueta = element(by.css('[href="/modulos/recinto/comercial/presenters/grids/etiqueta"]'));
    this.cadRestricao = element(by.css('[href="/modulos/recinto/comercial/presenters/grids/restricao"]'));
    this.cadPorto = element(by.css('[href="/modulos/recinto/comercial/presenters/grids/porto"]'));
    this.cadEmbarcacao = element(by.css('[href="/modulos/recinto/comercial/presenters/grids/embarcacao"]'));
    this.cadProduto = element(by.css('[href="/modulos/recinto/comercial/presenters/grids/categoria"]'));
    this.cadBudget = element(by.css('[href="/modulos/recinto/comercial/presenters/grids/budget"]'));
    this.cadProgramacao = element(by.css('[href="/modulos/recinto/comercial/presenters/grids/programacao"]'));

    //Elementos sub menu Gestão e Cadastros
    this.pessoa = element(by.css('[href="#Pessoa"]'));
    this.endereco = element(by.css('[href="Endereço"]'));

    //Elementos itens subs menu Gestão e Cadastros / Pessoa
    this.pessoaFisica = element(by.css('[href="/modulos/recinto/gestao_cadastros/presenters/grids/pessoa-fisica"]'));
    this.pessoaJuridica = element(by.css('[href="/modulos/recinto/gestao_cadastros/presenters/grids/pessoa-juridica"]'));
    this.pessoaEstrangeira = element(by.css('[href="/modulos/recinto/gestao_cadastros/presenters/grids/pessoa-estrangeira"]'));

    //Elementos itens subs menu Gestão e Cadastros / Endereço
    this.cidade = element(by.css('[href="/modulos/recinto/gestao_cadastros/presenters/grids/cidade"]'));
    this.estado = element(by.css('[href="/modulos/recinto/gestao_cadastros/presenters/grids/estado"]'));
    this.pais = element(by.css('[href="/modulos/recinto/gestao_cadastros/presenters/grids/pais"]'));

    //Elementos de acesso a operações das listas
    this.opIncluir = element(by.id('add-button-container'));
    this.opEditar = element(by.css(['title="Editar"']));
    this.opExcluir = element(by.css(['title="Excluir"']));
    this.opLog = element(by.css(['title="Log"']));
    this.opAprovar = element(by.css(['title="Aprovar"']));
    this.opReprovar = element(by.css(['title="Reprovar"']));

    this.acessaCadProgramacao = function(){
        this.menuPrincipal.click();
        this.programacao.click();
        this.cadProgramacao.click();
    };

    this.acessaCadEtiqueta = function(){
        this.menuPrincipal.click();
        this.cadastros.click();
        this.cadEtiqueta.click();
    };

    this.acessaCadRestricao = function(){
        this.menuPrincipal.click();
        this.cadastros.click();
        this.cadRestricao.click();
    };

    this.acessaCadPorto = function(){
        this.menuPrincipal.click();
        this.cadastros.click();
        this.cadPorto.click();
    };

    this.acessaCadEmbarcacao = function(){
        this.menuPrincipal.click();
        this.cadastros.click();
        this.cadEmbarcacao.click();
    };

    this.acessaCadProduto = function(){
        this.menuPrincipal.click();
        this.cadastros.click();
        this.cadProduto.click();
    };

    this.acessaCadBudget = function(){
        this.menuPrincipal.click();
        this.cadastros.click();
        this.cadBudget.click();
    };

    this.acessaCadPessoaFisica = function(){
        this.menuGestaoCad.click();
        this.pessoa.click();
        this.pessoaFisica.click();
    }

    this.acessaCadPessoaJuridica = function(){
        this.menuGestaoCad.click();
        this.pessoa.click();
        this.pessoaJuridica.click();
    }

    this.acessaCadPessoaEstrangeira = function(){
        this.menuGestaoCad.click();
        this.pessoa.click();
        this.pessoaEstrangeira.click();
    }

    this.acessaCadCidade = function(){
        this.menuGestaoCad.click();
        this.endereco.click();
        this.cidade.click();
    }

    this.acessaCadEstado = function(){
        this.menuGestaoCad.click();
        this.endereco.click();
        this.estado.click();
    }

    this.acessaCadPais = function(){
        this.menuGestaoCad.click();
        this.endereco.click();
        this.pais.click();
    }

    this.acessaIncluir = function(){
        this.opIncluir.click();
    };

    this.acessaEditar = function(){
        this.opEditar = click();
    };

    this.acessaExcluir = function(){
        this.opExcluir.click();
    };

};

module.exports = AcessoPage;