'use strict';

var HelpComponent = require('../../../../helpers/HelpComponent.js');

var PessoaFisicaPage = function(){

    var aux = new HelpComponent();

    this.itemComboBox = '/html/body/div[2]/div';

    this.cpf = element(by.name('cnpj_cpf_cuit'));
    this.nome = element(by.name('nome'));
    this.data_nasc = element.all(by.className('dx-texteditor-input')).get(2);
    this.orgao_emissor = element(by.name('orgao_emissor'));

    this.cep = element(by.name('cep'));
    this.endereco = element(by.name('endereco'));
    this.complemento = element(by.name('complemento'));
    this.bairro = element(by.name('bairro'));
    this.cidade = element(by.name('cidade.nome'));

    this.tel_comercial = element(by.name('tel_comercial'));
    this.tel_celular = element(by.name('tel_celular'));
    this.email = element(by.name('email'));

    this.insereModalidade = function(modalidadeIndex){
        aux.selectItemComboBox(element(by.id()), this.itemComboBox, modalidadeIndex);
        element(); //botão adicionar
    }

    this.btnSalvar = element(by.name('salvar'));

    this.inserePessoaFisica = function(cpf, nome, data_nasc, orgao_emissor, cep, endereco, complemento, bairro, cidade, tel_comercial, tel_celular,email, modalidadeIndex){
        this.cpf.sendKeys(cpf);
        this.nome.sendKeys(nome);
        this.data_nasc.sendKeys(data_nasc);
        this.orgao_emissor.sendKeys(orgao_emissor);
        this.cep.sendKeys(cep);
        this.endereco.sendKeys(endereco);
        this.complemento.sendKeys(complemento);
        this.bairro.sendKeys(bairro);
        this.cidade.sendKeys(cidade);
        this.tel_comercial.sendKeys(tel_comercial);
        this.tel_celular.sendKeys(tel_celular);
        this.email.sendKeys(email);
        //this.insereModalidade(modalidadeIndex);
    }

}

module.exports = PessoaFisicaPage;