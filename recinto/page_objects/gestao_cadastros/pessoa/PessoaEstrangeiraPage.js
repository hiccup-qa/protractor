'use strict';

var HelpComponent = require('../../../../helpers/HelpComponent.js');

var PessoaEstrangeiraPage = function(){

    var aux = new HelpComponent();

    this.itemComboBox = '/html/body/div[2]/div';

    this.passaporte = element(by.name('cnpj_cpf_cuit'));
    this.orgao_emissor = element(by.name('orgao_emissor'));
    this.nome = element(by.name('nome'));
    this.data_nasc = element.all(by.className('dx-texteditor-input')).get(3);

    this.cep = element(by.name('cep'));
    this.endereco = element(by.name('endereco'));
    this.complemento = element(by.name('complemento'));
    this.bairro = element(by.name('bairro'));
    this.cidade = element(by.name('cidade.nome'));

    this.tel_comercial = element(by.name('tel_comercial'));
    this.tel_celular = element(by.name('tel_celular'));
    this.email = element(by.name('email'));

    this.modalidade = element.all(by.className('dx-texteditor-input')).get(14);

    this.insereModalidade = function(modalidadeIndex){
        aux.selectItemComboBox(this.modalidade, this.itemComboBox, modalidadeIndex);
    }

    this.btnSalvar = element(by.name('salvar'));

    this.inserePessoaEstrangeira = function(passaporte, orgao_emissor, nome, data_nasc, cep, endereco, complemento, bairro, cidade, tel_comercial, tel_celular,email, modalidadeIndex){
        
        this.passaporte.sendKeys(passaporte);
        this.orgao_emissor.sendKeys(orgao_emissor);
        this.nome.sendKeys(nome);
        this.data_nasc.sendKeys(data_nasc);
        this.cep.sendKeys(cep);
        this.endereco.sendKeys(endereco);
        this.complemento.sendKeys(complemento);
        this.bairro.sendKeys(bairro);
        aux.selectItemPopUp(this.cidade,cidade,this.itemComboBox);
        this.tel_comercial.sendKeys(tel_comercial);
        this.tel_celular.sendKeys(tel_celular);
        this.email.sendKeys(email);
        this.insereModalidade(modalidadeIndex);
        this.btnSalvar.click();
    }

}

module.exports = PessoaEstrangeiraPage;