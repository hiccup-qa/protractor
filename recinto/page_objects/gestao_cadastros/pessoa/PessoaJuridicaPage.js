'use strict';

var HelpComponent = require('../../../../helpers/HelpComponent.js');

var PessoaJuridicaPage = function(){

    var aux = new HelpComponent();

    this.itemComboBox = '/html/body/div[2]/div';

    this.cnpj = element(by.name('cnpj_cpf_cuit'));
    this.razao_social = element(by.name('nome'));
    this.nome_fantasia = element(by.name('nome_fantasia'));
    this.ie = element(by.name('ie_rg'));

    this.cep = element(by.name('cep'));
    this.endereco = element(by.name('endereco'));
    this.complemento = element(by.name('complemento'));
    this.bairro = element(by.name('bairro'));
    this.cidade = element(by.name('cidade.nome'));

    this.tel_comercial = element(by.name('tel_comercial'));
    this.tel_celular = element(by.name('tel_celular'));
    this.email = element(by.name('email'));

    this.insereModalidade = function(modalidadeIndex){
        aux.selectItemComboBox(element(by.id()), this.itemComboBox, modalidadeIndex);
        element(); //botão adicionar
    }

    this.btnSalvar = element(by.name('salvar'));

    this.inserePessoaJuridica = function(cnpj, razao_social, nome_fantasia, ie, cep, endereco, complemento, bairro, cidade, tel_comercial, tel_celular,email, modalidadeIndex){
        this.cnpj.sendKeys(cnpj);
        this.razao_social.sendKeys(razao_social);
        this.nome_fantasia.sendKeys(nome_fantasia);
        this.ie.sendKeys(ie);
        this.cep.sendKeys(cep);
        this.endereco.sendKeys(endereco);
        this.complemento.sendKeys(complemento);
        this.bairro.sendKeys(bairro);
        this.cidade.sendKeys(cidade);
        this.tel_comercial.sendKeys(tel_comercial);
        this.tel_celular.sendKeys(tel_celular);
        this.email.sendKeys(email);
        //this.insereModalidade(modalidadeIndex);
    }

}

module.exports = PessoaJuridicaPage;