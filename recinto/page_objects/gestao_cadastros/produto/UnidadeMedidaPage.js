'use strict';

var UnidadeMedidaPage = function(){
    this.abreviacao = element(by.name('abreviacao'));
    this.descricao = elemen(by.name('descricao'));
    this.btnSalvar = element(by.name('salvar'));

    this.insereUnidadeMedida = function(abreviacao, descricao){
        this.abreviacao.sendKeys(abreviacao);
        this.descricao.sendKeys(descricao);
        this.btnSalvar.click();
    };
}

module.exports = UnidadeMedidaPage;