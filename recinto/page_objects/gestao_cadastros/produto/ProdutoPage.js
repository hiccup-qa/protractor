'use strict';

var HelpComponent = require('../../../../helpers/HelpComponent.js');

var ProdutoPage = function(){

    var aux = new HelpComponent();
    this.itemComboBox = '/html/body/div[2]/div';

    this.produto = element(by.name('descricao'));
    this.categoria = element.all(by.className('dx-text-editor')).get(1);
    this.ncm = element(by.name('ncm'));
    this.unidadeMedida = element(by.name('unidade_medida_descricao'));
    this.erp = element(by.name('erp'));
    this.obs = element(by.name('observacoes'));
    this.btnSalvar = element(by.name('salvar'));

    this.insereProduto = function(produto, ncm, unidadeMedida, erp, obs){
        this.produto.sendKeys(produto);
        aux.selectItemComboBox(this.categoria,this.itemComboBox,0);
        this.ncm.sendKeys(ncm);
        aux.selectItemPopUp(this.unidadeMedida,unidadeMedida, 0);
        this.erp.sendKeys(erp);
        this.obs.sendKeys(obs);
        this.btnSalvar.click();
    }

}

module.exports = ProdutoPage;