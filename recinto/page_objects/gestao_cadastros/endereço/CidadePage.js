'use strict';

var HelpComponent = require('../../../../helpers/HelpComponent.js');

var CidadePage = function(){

    var aux = new HelpComponent();
    this.itemComboBox = '/html/body/div[2]/div';

    this.cidade = element(by.name('nome'));
    this.sigla = element(by.name('sigla'));
    this.estado = element.all(by.className('dx-texteditor-input')).get(2);
    this.cep = element(by.name('cep'));
    this.btnSalvar = element(by.name('salvar'));

    this.insereCidade = function(cidade, sigla, cep){
        this.cidade.sendKeys(cidade);
        this.sigla.sendKeys(sigla);
        aux.selectItemComboBox(this.estado, this.itemComboBox, 0);
        this.cep.sendKeys(cep);
        this.btnSalvar.click();
    }
}

module.exports = CidadePage;