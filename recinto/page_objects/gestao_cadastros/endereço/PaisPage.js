'use strict';

var PaisPage = function(){

    this.descricao = element(by.name('descricao'));
    this.desc_portugues = element(by.name('descricao_portugues'));
    this.btnSalvar = element(by.name('salvar'));

    this.inserePais = function(descricao, desc_portugues){
        this.descricao.sendKeys(descricao);
        this.desc_portugues.sendKeys(desc_portugues);
        btnSalvar.click();
    }
}

module.exports = PaisPage;