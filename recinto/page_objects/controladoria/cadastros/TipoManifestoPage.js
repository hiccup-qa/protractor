'use strict';

var TipoManifestoPage = function(){
    this.descricao = element(by.name('descricao'));
    this.observacao = element(by.name('observacao'));
    this.btnSalvar = element(by.name('salvar'));

    this.insereTipoManifesto = function(descricao, observacao){
        this.descricao.sendKeys(descricao);
        this.observacao.sendKeys(observacao);
        this.salvar.click();
    }
}


module.exports = TipoManifestoPage;