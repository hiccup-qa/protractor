'use strict';

var ContratoGlobalPage = function(){

    this.cnpj = element(by.name('cnpj_raiz'));
    this.cliente = elemtn(by.name('nome_cliente'));
    this.validade = element.all(by.className('dx-texteditor-input')).get(2);
    this.dadosContrato = element.all(by.className('ql-container ql-snow')).get(0);
    this.btnSalvar = element(by.name('salvar'));

    this.insereContratoGlobal = function(cnpj, cliente, validade, dadosContrato){
        this.cnpj.sendKeys(cnpj);
        this.cliente.sendKeys(cliente);
        this.validade.sendKeys(validade);
        this.dadosContrato.sendKeys(dadosContrato);
        this.btnSalvar.click();
    }
}

module.exports = ContratoGlobalPage;