'use strict';

var HelpComponent = require('../../../../helpers/HelpComponent.js');

var EmbarcacaoPage = function(){

    var aux = new HelpComponent();

    this.itemComboBox = '/html/body/div[2]/div';
    this.flagNome = 'Brasil';
    this.portoNome = 'Paranaguá';
    
    this.vesselName = element(by.name('vessel_name'));
    this.vesselNameEx = element(by.name('vessel_ex_name'));
    this.ano = element.all(by.className('dx-texteditor-input')).get(2);
    this.tipo = element(by.id('embarcacao-form-tipo'));
    this.imo = element.all(by.className('dx-texteditor-input')).get(4);
    this.gear = element(by.name('gear')); //5
    this.flag = element(by.name('bandeira'));
    this.callSign = element(by.name('call_sign')); //7
    this.porto = element(by.name('porto_registro'));
    this.dwt = element.all(by.className('dx-texteditor-input')).get(9);
    this.grt = element.all(by.className('dx-texteditor-input')).get(10);
    this.loa = element.all(by.className('dx-texteditor-input')).get(11);
    this.bale = element.all(by.className('dx-texteditor-input')).get(12);
    this.grain = element.all(by.className('dx-texteditor-input')).get(13);
    this.gangway = element(by.name('gangway'));
    this.observacao = element.all(by.className('dx-texteditor-input')).get(15);
    this.beam = element.all(by.className('dx-texteditor-input')).get(16);
    this.outreach = element.all(by.className('dx-texteditor-input')).get(17);
    this.net = element.all(by.className('dx-texteditor-input')).get(18);
    this.charterers = element(by.name('charterers'));

    this.btnSalvar = element(by.name('salvar'));
    this.btnCancelar = element(by.name('cancelar'));

    this.abaIdentificacao = element.all(by.className('dx-item dx-tab')).get(0);
    this.abaInfoEmbarcacao = element.all(by.className('dx-item dx-tab')).get(1);
    this.btlIncluirPorao = element(by.id('embarcacao-form-add-porao'));
    this.btnSalvarPorao = element(by.id('salvar-porao'));
    this.btnCancelarPorao = element(by.id('cancelar-porao'));

    this.numPorao = element.all(by.className('dx-texteditor-input')).get(20);
    this.comprimentoPorao = element.all(by.className('dx-texteditor-input')).get(21);
    this.larguraPorao = element.all(by.className('dx-texteditor-input')).get(22);
    this.comprimentoBoca = element.all(by.className('dx-texteditor-input')).get(23);
    this.larguraBoca = element.all(by.className('dx-texteditor-input')).get(24);
    this.observacaoPorao = element.all(by.className('dx-texteditor-input')).get(25);

    this.geraDescAutomatica = function(string , numeroConcat){
        return string + ' - ' + Math.floor(Math.random() * numeroConcat);
    };

    this.geraNum = function(tamanho){
        return Math.floor(Math.random() * tamanho);
    };

    this.inserePorao = function(){
        this.btlIncluirPorao.click();
        this.numPorao.sendKeys(this.geraNum(1000));
        this.comprimentoPorao.sendKeys(this.geraNum(20));
        this.larguraPorao.sendKeys(this.geraNum(20));
        this.comprimentoBoca.sendKeys(this.geraNum(10));
        this.larguraBoca.sendKeys(this.geraNum(10));
        this.observacaoPorao.sendKeys(this.geraDescAutomatica('Observação teste automatizado', 100000));
        this.btnSalvarPorao.click();
    };

    this.insereEmbarcacao = function(){
        this.vesselName.sendKeys(this.geraDescAutomatica('Vessel name automatizado', 999999));
        this.vesselNameEx.sendKeys(this.geraDescAutomatica('Vessel name ex automatizado', 9999));
        this.ano.sendKeys(2000);
        aux.selectItemComboBox(this.tipo, this.itemComboBox, 0);
        this.imo.sendKeys(this.geraNum(9999999));
        aux.selectItemPopUp(this.flag, this.flagNome, this.itemComboBox);
        this.gear.sendKeys(this.geraNum(9999));
        aux.selectItemPopUp(this.porto, this.portoNome, this.itemComboBox);
        this.callSign.sendKeys(this.geraDescAutomatica('Callsign automatizada'), 9999);
        this.dwt.sendKeys(this.geraNum(50));
        this.grt.sendKeys(this.geraNum(99));
        this.loa.sendKeys(this.geraNum(99));
        this.bale.sendKeys(this.geraNum(99));
        this.grain.sendKeys(this.geraNum(99));
        this.gangway.sendKeys(this.geraDescAutomatica('Gangway automatizado'), 9999);
        this.observacao.sendKeys(this.geraDescAutomatica('Observação teste automatizado'), 999999999);

        this.abaInfoEmbarcacao.click();
        
        this.beam.sendKeys(this.geraNum(99));
        this.outreach.sendKeys(123456789);
        this.net.sendKeys(this.geraNum(99));
        this.charterers.sendKeys(this.geraNum(99));
        
        this.inserePorao();

        this.btnSalvar.click();
    };

    

};

module.exports = EmbarcacaoPage;