'use strict';

var MecanismoPage = function(){

    this.descricao = element(by.name('descricao'));
    this.btnSalvar = element(by.name('salvar'));

    this.insereMecanismo = function(descricao){
        this.descricao.sendKeys(descricao);
        this.btnSalvar.click();
    };

};

module.exports = MecanismoPage;