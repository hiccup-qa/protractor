'use strict';

var BudgetPage = function(){

    this.descricao = element(by.name('descricao'));
    this.quantidade = element.all(by.className('dx-texteditor-input')).get(10);
    this.data = element.all(by.className('dx-texteditor-input')).get(11);
    this.btnSalvar = element(by.name('salvar'));

    this.descricaoFiltro = element.all(by.className('dx-texteditor-input')).get(2);  

    this.insereBudget = function(descricao, quantidade, data){
        this.descricao.sendKeys(descricao);
        this.quantidade.sendKeys(quantidade);
        this.data.sendKeys(data);
        this.btnSalvar.click();
    };

    this.editaBudget = function(descAntiga, descNova, qtdNova, dataNova){
        element(by.id('atualizar')).click();
        this.descricaoFiltro.sendKeys(descAntiga);
        element(by.css('[title="Editar"]')).click();
        this.descricao.clear();
        this.descricao.sendKeys(descNova);
        this.quantidade.clear();
        this.quantidade.sendKeys(qtdNova);
        this.data.clear();
        this.data.sendKeys(dataNova);
        this.btnSalvar.click();
        
    };

    this.excluirBudget = function(descBudget, usuario, senha, motivo){
        this.descricaoFiltro.sendKeys(descBudget);
        element(by.css('[title="Excluir"]')).click();
        element(by.css('[aria-label="Sim"]')).click();
        element(by.name('username')).sendKeys(usuario);
        element(by.name('password')).sendKeys(senha);
        element(by.name('motivo')).sendKeys(motivo);
        element(by.cssContainingText('.dx-button-text', 'CONFIRMAR')).click();
    };

};

module.exports = BudgetPage;