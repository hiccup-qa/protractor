'use strict';

var HelpComponent = require('../../../../helpers/HelpComponent.js');

var EtiquetaPage = function(){

    var aux = new HelpComponent();

    this.itemComboBox = '/html/body/div[3]/div';

    this.descricao = element(by.name('descricao'));
    this.tipo = element(by.id('etiqueta-form-tipo_id'));
    this.cor = element(by.name('cor'));
    this.btnSalvar = element(by.name('salvar'));
    this.btnCancelar = element(by.name('cancelar'));

    this.descricaoFiltro = element.all(by.className('dx-texteditor-input')).get(2);  

    this.insereEtiqueta = function(descricao, cor){
        this.descricao.sendKeys(descricao);
        aux.selectItemComboBox(this.tipo, this.itemComboBox, 0);
        this.cor.sendKeys(cor);
        this.btnSalvar.click();
    };

    this.editaEtiqueta = function(antigaDesc, novaDesc, novaCor){
        this.descricaoFiltro.sendKeys(antigaDesc);
        element(by.css('[title="Editar"]')).click();
        this.descricao.clear();
        this.descricao.sendKeys(novaDesc);
        this.cor.clear();
        this.cor.sendKeys(novaCor);
        this.btnSalvar.click();
    };

    this.excluirEtiqueta = function(descEtiqueta, usuario, senha, motivo){
        this.descricaoFiltro.sendKeys(descEtiqueta);
        element(by.css('[title="Excluir"]')).click();
        element(by.css('[aria-label="Sim"]')).click();
        element(by.name('username')).sendKeys(usuario);
        element(by.name('password')).sendKeys(senha);
        element(by.name('motivo')).sendKeys(motivo);
        element(by.cssContainingText('.dx-button-text', 'CONFIRMAR')).click();
    };
};

module.exports = EtiquetaPage;