'use strict';

var HelpComponent = require('../../../../helpers/HelpComponent.js');

var PortoPage = function(){

    var aux = new HelpComponent();

    this.itemComboBox = '/html/body/div[3]/div';

    this.nome = element(by.name('nome'));
    this.pais = element(by.name('pais_nome'));
    this.bigrama = element(by.name('bigrama'));
    this.trigrama = element(by.name('trigrama'));
    this.btnSalvar = element(by.name('salvar'));

    this.descricaoFiltro = element.all(by.className('dx-texteditor-input')).get(2);

    this.inserePorto = function(porto, paisNome, bigrama, trigrama){
        this.nome.sendKeys(porto);
        aux.selectItemPopUp(this.pais, paisNome, this.itemComboBox);
        this.bigrama.sendKeys(bigrama);
        this.trigrama.sendKeys(trigrama);
        this.btnSalvar.click();
    };

    this.editaPorto = function(antigoPorto, novoPorto, novoPais, novoBigrama, novoTrigrama){
        this.descricaoFiltro.sendKeys(antigoPorto);
        element(by.css('[title="Editar"]')).click();
        this.nome.clear();
        this.nome.sendKeys(novoPorto);
        browser.sleep(2000);
        this.pais.clear();
        browser.sleep(2000);
        aux.selectItemPopUp(this.pais, novoPais, this.itemComboBox);
        this.bigrama.clear();
        this.bigrama.sendKeys(novoBigrama);
        this.trigrama.clear();
        this.trigrama.sendKeys(novoTrigrama);
        this.btnSalvar.click();
    };

    this.excluirPorto = function(descPorto, usuario, senha, motivo){
        this.descricaoFiltro.sendKeys(descPorto);
        element(by.css('[title="Excluir"]')).click();
        element(by.css('[aria-label="Sim"]')).click();
        element(by.name('username')).sendKeys(usuario);
        element(by.name('password')).sendKeys(senha);
        element(by.name('motivo')).sendKeys(motivo);
        element(by.cssContainingText('.dx-button-text', 'CONFIRMAR')).click();
    };

};

module.exports = PortoPage;