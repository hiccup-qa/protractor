'use strict';

var HelpComponent = require('../../../../helpers/HelpComponent.js');

var RestricaoPage = function(){

    var aux = new HelpComponent();

    this.itemComboBox = '/html/body/div[3]/div';

    this.produto = element(by.name('categoria_produto'));
    this.porto = element(by.name('porto_origem'));
    this.btnSalvar = element(by.name('salvar'));
    this.btnCancelar = element(by.name('cancelar'));

    this.filtraRestricao = function(produto, porto, pais){
        element.all(by.className('dx-texteditor-input')).get(2).sendKeys(produto);
        element.all(by.className('dx-texteditor-input')).get(3).sendKeys(porto);
        element.all(by.className('dx-texteditor-input')).get(4).sendKeys(pais);
    };

    this.insereRestTodos = function(produto, porto){
        aux.selectItemPopUp(this.produto, produto, this.itemComboBox);
        aux.selectItemPopUp(this.porto, porto, this.itemComboBox);
        this.btnSalvar.click();
    };

    this.editaRestricao = function(produtoAntigo, produtoNovo, portoAntigo, portoNovo, paisAntigo){
        this.filtraRestricao(produtoAntigo, portoAntigo, paisAntigo);
        element(by.css('[title="Editar"]')).click();
        this.produto.clear();
        aux.selectItemPopUp(this.produto, produtoNovo, this.itemComboBox);
        this.porto.clear();
        aux.selectItemPopUp(this.porto, portoNovo, this.itemComboBox);
        this.btnSalvar.click();
    };

    this.excluirRestricao = function(produtoNovo, portoNovo, paisNovo, usuario, senha, motivo){
        this.filtraRestricao(produtoNovo, portoNovo, paisNovo);
        element(by.css('[title="Excluir"]')).click();
        element(by.css('[aria-label="Sim"]')).click();
        element(by.name('username')).sendKeys(usuario);
        element(by.name('password')).sendKeys(senha);
        element(by.name('motivo')).sendKeys(motivo);
        element(by.cssContainingText('.dx-button-text', 'CONFIRMAR')).click();
    };

};

module.exports = RestricaoPage;