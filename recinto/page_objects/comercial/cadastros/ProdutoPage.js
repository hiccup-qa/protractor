'use strict';

var ProdutoPage = function(){

    this.descricao = element(by.name('descricao'));
    this.btnSalvar = element(by.name('salvar'));

    this.descricaoFiltro = element.all(by.className('dx-texteditor-input')).get(2);

    this.insereProdutoAtivo = function(descricao){
        this.descricao.sendKeys(descricao);
        this.btnSalvar.click();
    };

    this.editaProduto = function(antigaDesc, novaDesc){
        this.descricaoFiltro.sendKeys(antigaDesc);
        element(by.css('[title="Editar"]')).click();
        this.descricao.clear();
        this.descricao.sendKeys(novaDesc);
        this.btnSalvar.click();
    }

    this.excluirProduto = function(descProduto, usuario, senha, motivo){
        this.descricaoFiltro.sendKeys(descProduto);
        element(by.css('[title="Excluir"]')).click();
        element(by.css('[aria-label="Sim"]')).click();
        element(by.name('username')).sendKeys(usuario);
        element(by.name('password')).sendKeys(senha);
        element(by.name('motivo')).sendKeys(motivo);
        element(by.cssContainingText('.dx-button-text', 'CONFIRMAR')).click();
    };


};

module.exports = ProdutoPage;