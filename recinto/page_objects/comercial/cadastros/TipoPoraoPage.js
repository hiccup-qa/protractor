'use strict';

var TipoPoraoPage = function(){

    this.descricao = element(by.name('descricao'));
    this.btnSalvar = element(by.name('salvar'));

    this.insereTipoPorao = function(descricao){
        this.descricao.sendKeys(descricao);
        this.btnSalvar.click();
    };

};

module.exports = TipoPoraoPage;