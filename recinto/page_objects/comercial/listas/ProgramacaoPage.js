'use strict';

var HelpComponent = require('../../../../helpers/HelpComponent.js');

var CadProgramacaoPage = function(){

    var aux = new HelpComponent();

    this.itemComboBox = '/html/body/div[2]/div';
    this.itemComboBoxModal = '/html/body/div[3]/div';
    this.portoNome = 'Paranaguá';
    
    this.berco = element(by.id('programacao-form-berco'));
    this.eta = element.all(by.className('dx-texteditor-input')).get(2);
    this.etb = element.all(by.className('dx-texteditor-input')).get(3);
    this.ets = element.all(by.className('dx-texteditor-input')).get(4);
    this.agencia =  element(by.id('programacao-form-agencia'));
    this.porto = element.all(by.className('dx-texteditor-input')).get(6);
    this.btnImportador = element(by.name('toggle-importador'));
    this.btnExportador = element(by.name('toggle-exportador'));
    this.btnAdcionar = element(by.name('adicionar-impexp'));
    this.qtdeTotal = element.all(by.className('dx-texteditor-input')).get(8);
    this.observacao = element.all(by.className('dx-texteditor-input')).get(9);
    this.btnCancelar = element(by.name('cancelar'));
    this.btnSalvar = element(by.name('salvar'));

    //Elementos do modal
    this.etiqueta = element(by.id('impexp-etiqueta'));
    this.qtdeDescarga = element.all(by.className('dx-texteditor-input')).get(8);
    this.modalObs = element.all(by.className('dx-texteditor-input')).get(9);
    this.categoria = element(by.id('impexp-produto'));
    this.modalCancelar = element(by.name('impexp-cancelar'));
    this.modalSalvar = element(by.name('impexp-salvar'));

    /*
    this.selecionaEmbarcacao = function(){
        element(by.id('programacao-form-embarcacao')).click();
        element.all(by.xpath(itemComboBox)).first().click();
    };

    this.selecionaEtiqueta = function(){
        browser.ignoreSynchronization = true;
        element(by.id('impexp-etiqueta')).click();
        browser.wait(EC.visibilityOf(element(by.xpath(itemComboBoxModal))),30000,"Não foi possivel selelcionar o item da caixa de seleção");
        element.all(by.xpath(itemComboBox)).first().click();
        browser.ignoreSynchronization = false;
    };
    */
    this.insereImportador = function(){
        this.btnImportador.click();
        this.btnAdcionar.click();
        this.selecionaEtiqueta();
        this.qtdeDescarga.sendKeys(Math.floor(Math.random() * 100));
        this.modalObs.sendKeys('Observação teste automatizado');
        browser.sleep(10000);
        //this.modalSalvar.click();
    };

    this.insereExportador = function(){
        this.btnExportador.click();
        this.btnAdcionar.click();
        this.selecionaEtiqueta();
        this.qtdeDescarga.sendKeys(Math.floor(Math.random() * 100));
        this.modalObs.sendKeys('Observação teste automatizado');
        this.modalSalvar.click();
    };

    this.insereJanela = function(){

        var diaEta = 10;
        var mes = Math.floor(Math.random() * 11) + 1;
        var ano = new Date().getFullYear() + Math.floor(Math.random() * 50);
        var diaEtb = 13;
        var diaEts = 16;

        this.eta.sendKeys(diaEta.toString()+'/'+mes.toString()+'/'+ano.toString());
        this.etb.sendKeys(diaEtb.toString()+'/'+mes.toString()+'/'+ano.toString());
        this.ets.sendKeys(diaEts.toString()+'/'+mes.toString()+'/'+ano.toString());

    }

    this.insereProgramacao = function(){
        browser.sleep(10000);
        aux.selectItemComboBox(this.berco, this.itemComboBox,0);
        this.insereJanela();
        aux.selectItemComboBox(this.agencia, this.itemComboBox,0);
        aux.selectItemPopUp(this.porto, this.portoNome, this.itemComboBox);
        this.qtdeTotal.sendKeys(Math.floor(Math.random() * 100));
        this.observacao.sendKeys('Observação teste automatizado');
        this.btnSalvar.click();
    };

};

module.exports = CadProgramacaoPage;