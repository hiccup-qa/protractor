'use strict';

var EtiquetaPage = require('../page_objects/comercial/cadastros/EtiquetaPage.js');
var HelpComponent = require('../../helpers/HelpComponent.js');
var Validador = require('../../helpers/ValidaPage.js');
var LoginPage = require('../helpers/LoginPage.js');
var Acessos = require('../helpers/AcessoPage.js');

describe('Regressão CRUD do cadastro de etiqueta', function(){

    var page = new EtiquetaPage();
    var init = new LoginPage();
    var aux = new HelpComponent();
    var validador = new Validador();
    var user = new Acessos();

    var etiqueta = {
        antiga : aux.geraString('Etiqueta'),
        nova : aux.geraString('Nova etiqueta')
    };

    beforeEach(function(){
        browser.driver.manage().window().maximize();
        browser.get('http://o01b18.kmm.com.br:8000/modulos/recinto'); 
    });

    afterEach(function(){
        browser.ignoreSynchronization = false;
    });

    it('Deveria inserir uma etiqueta', function(){
        init.logar('admincorp', 'kmm2018', true);
        user.acessaCadEtiqueta();
        user.acessaIncluir();
        page.insereEtiqueta(etiqueta.antiga, '#FFFFFF');
        expect(validador.validaSucesso()).toContain('dx-toast-success');
    });

    it('Deveria alterar uma etiqueta', function(){
        user.acessaCadEtiqueta();
        page.editaEtiqueta(etiqueta.antiga, etiqueta.nova, '#74DF00');
        expect(validador.validaSucesso()).toContain('dx-toast-success');
    });

    it('Deveria excluir uma etiqueta', function(){
        user.acessaCadEtiqueta();
        page.excluirEtiqueta(etiqueta.nova, 'admincorp', 'kmm2018', 'Aprovado pelo teste automatizado');
        expect(validador.validaSucesso()).toContain('dx-toast-success');
    });
});