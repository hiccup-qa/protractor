'use strict';

var PortoPage = require('../page_objects/comercial/cadastros/PortoPage.js');
var HelpComponent = require('../../helpers/HelpComponent.js');
var Validador = require('../../helpers/ValidaPage.js');
var LoginPage = require('../helpers/LoginPage.js');
var Acessos = require('../helpers/AcessoPage.js');

describe('Regressão CRUD do cadastro de porto', function(){

    var page = new PortoPage();
    var init = new LoginPage();
    var aux = new HelpComponent();
    var validador = new Validador();
    var user = new Acessos();

    var porto = {
        antigo : aux.geraString('Porto'),
        novo : aux.geraString('Novo porto')
    };

    beforeEach(function(){
        browser.driver.manage().window().maximize();
        browser.get('http://o01b18.kmm.com.br:8000/modulos/recinto'); 
    });

    afterEach(function(){
        browser.ignoreSynchronization = false;
    });
    
    it('Deveria inserir um porto', function(){
        init.logar('admincorp', 'kmm2018', true);
        user.acessaCadPorto();
        user.acessaIncluir();
        page.inserePorto(porto.antigo, 'Brasil',aux.geraStringRandom(2), aux.geraStringRandom(3));
        expect(validador.validaSucesso()).toContain('dx-toast-success');
    });

    it('Deveria alterar um porto', function(){
        user.acessaCadPorto();
        page.editaPorto(porto.antigo, porto.novo, 'Argentina',aux.geraStringRandom(2), aux.geraStringRandom(3));
        expect(validador.validaSucesso()).toContain('dx-toast-success');
    });

    it('Deveria excluir um porto', function(){
        user.acessaCadPorto();
        page.excluirPorto(porto.novo, 'admincorp', 'kmm2018', 'Aprovado pelo teste automatizado');
        expect(validador.validaSucesso()).toContain('dx-toast-success');
    });
});