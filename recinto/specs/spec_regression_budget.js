'use strict';

var BudgetPage = require('../page_objects/comercial/cadastros/BudgetPage.js');
var HelpComponent = require('../../helpers/HelpComponent.js');
var Validador = require('../../helpers/ValidaPage.js');
var LoginPage = require('../helpers/LoginPage.js');
var Acessos = require('../helpers/AcessoPage.js');

describe('Regressão CRUD do cadastro de budget', function(){

    var page = new BudgetPage();
    var init = new LoginPage();
    var aux = new HelpComponent();
    var validador = new Validador();
    var user = new Acessos();

    var budget = {
        antigo : aux.geraString('BUDGET'),
        novo : aux.geraString('NOVO BUDGET')
    };

    beforeEach(function(){
        browser.driver.manage().window().maximize();
        browser.get('http://o01b18.kmm.com.br:8000/modulos/recinto'); 
    });

    afterEach(function(){
        browser.ignoreSynchronization = false;
    });

    it('Deveria inserir um budget', function(){
        init.logar('admincorp', 'kmm2018', true);
        user.acessaCadBudget();
        user.acessaIncluir();
        page.insereBudget(budget.antigo, aux.geraNum(1000), aux.geraDateRandom());
        expect(validador.validaSucesso()).toContain('dx-toast-success');
    });

    it('Deveria alterar um budget', function(){
        user.acessaCadBudget();
        page.editaBudget(budget.antigo, budget.novo, aux.geraNum(1000), aux.geraDateRandom());
        expect(validador.validaSucesso()).toContain('dx-toast-success');
    });

    it('Deveria excluir um budget', function(){
        user.acessaCadBudget();
        page.excluirBudget(budget.novo, 'admincorp', 'kmm2018', 'Aprovado pelo teste automatizado');
        expect(validador.validaSucesso()).toContain('dx-toast-success');
    });

});