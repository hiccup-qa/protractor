'use strict';

var LoginPage = require('../page_objects/comercial/cadastros/LoginPage.js');
var AcessoPage = require('../helpers/AcessoPage.js');
var EtiquetaPage = require('../page_objects/comercial/cadastros/EtiquetaPage.js');
var PortoPage = require('../page_objects/comercial/cadastros/PortoPage.js');
var ProdutoPage = require('../page_objects/comercial/cadastros/ProdutoPage.js');
var BudgetPage = require('../page_objects/comercial/cadastros/BudgetPage.js');
var EmbarcacaoPage = require('../page_objects/comercial/cadastros/EmbarcacaoPage.js');
var ProgramacaoPage = require('../page_objects/comercial/listas/ProgramacaoPage.js');
var RestricaoPage = require('../page_objects/comercial/cadastros/RestricaoPage.js');
var ValidaPage = require('../../helpers/ValidaPage.js');
var HelpComponent = require('../../helpers/HelpComponent.js');


describe('Recinto - Cadastros', function(){

  var lgn = new LoginPage();
  var user = new AcessoPage();
  var etq = new EtiquetaPage();
  var prt = new PortoPage();
  var pdt = new ProdutoPage();
  var bgt = new BudgetPage();
  var emb = new EmbarcacaoPage();
  var prog = new ProgramacaoPage();
  var rest = new RestricaoPage();
  var validador = new ValidaPage();
  var aux = new HelpComponent();

  var produto = aux.geraString('Produto');

  beforeEach(function(){
    browser.driver.manage().window().maximize();
    browser.get('http://o01b18.kmm.com.br:8000/modulos/recinto'); 
  });

  afterEach(function(){
    browser.ignoreSynchronization = false;
  });

  it('Deveria cadastrar etiqueta', function(){
    lgn.logar('kmm_suporte','kmm2018', true);  
    user.acessaCadEtiqueta();
    user.acessaIncluir();
    etq.insereEtiqueta(aux.geraString('Etiqueta'), '#FFFFFF');
    expect(validador.validaSucesso()).toContain('dx-toast-success');
  });

  it('Deveria cadastrar porto', function(){
    user.acessaCadPorto();
    user.acessaIncluir();
    prt.inserePorto(aux.geraString('Porto'), aux.geraStringRandom(2), aux.geraStringRandom(3));
    expect(validador.validaSucesso()).toContain('dx-toast-success');
  });

  it('Deveria cadastrar categoria de produto', function(){
    user.acessaCadProduto();
    user.acessaIncluir();
    pdt.insereProdutoAtivo(produto);
    expect(validador.validaSucesso()).toContain('dx-toast-success');
  });
  
  it("Deveria cadastrar budget", function(){
    user.acessaCadBudget();
    user.acessaIncluir();
    bgt.insere_budget(aux.geraString('Budget'), aux.geraNum(1000), aux.geraDateRandom());
    expect(validador.validaSucesso()).toContain('dx-toast-success');
  });

 /*
  it('Deveria cadastrar embarcação', function(){
    user.acessaCadEmbarcacao();
    user.acessaIncluir();
    browser.sleep(100);
    emb.insereEmbarcacao();
    expect(validador.validaSucesso()).toContain('dx-toast-success');
  });

  it('Deveria cadastrar restrição', function(){
    user.acessaCadRestricao();
    user.acessaIncluir();
    browser.sleep(100);
    rest.insereRestTodos(produto,'Paranaguá');
    expect(validador.validaSucesso()).toContain('dx-toast-success');
  });

  it('Deveria cadastrar programação', function(){ 
    user.acessaCadProgramacao();
    user.acessaIncluir();
    browser.sleep(100);
    prog.insereProgramacao();
    expect(validador.validaSucesso()).toContain('dx-toast-success');
  });
  */
});

