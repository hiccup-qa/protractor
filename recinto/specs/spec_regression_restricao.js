'use strict';

var PortoPage = require('../page_objects/comercial/cadastros/PortoPage.js');
var ProdutoPage = require('../page_objects/comercial/cadastros/ProdutoPage.js');
var RestricaoPage = require('../page_objects/comercial/cadastros/RestricaoPage.js');
var HelpComponent = require('../../helpers/HelpComponent.js');
var Validador = require('../../helpers/ValidaPage.js');
var LoginPage = require('../helpers/LoginPage.js');
var Acessos = require('../helpers/AcessoPage.js');

describe('Regressão CRUD do cadastro de restrição', function(){

    var auxPageProduto = new ProdutoPage();
    var auxPagePorto = new PortoPage();
    var page = new RestricaoPage();
    var init = new LoginPage();
    var aux = new HelpComponent();
    var validador = new Validador();
    var user = new Acessos();

    var restricao = {
        produto : aux.geraString('Produto'),
        porto: aux.geraString('Porto'),
        pais : 'Brasil',
        produtoNovo : aux.geraString('Novo produto'),
        portoNovo : aux.geraString('Novo porto'),
        paisNovo : 'Argentina'
    };

    beforeEach(function(){
        browser.driver.manage().window().maximize();
        browser.get('http://o01b18.kmm.com.br:8000/modulos/recinto'); 
    });

    afterEach(function(){
        browser.ignoreSynchronization = false;
    });

    it('Deveria cadastrar produto para inserção da restrição', function(){
        init.logar('admincorp', 'kmm2018', true);
        //Cadastra os produtos utlizados no teste
        user.acessaCadProduto();
        user.acessaIncluir();
        console.log("PRODUTO = "+restricao.produto);
        auxPageProduto.insereProdutoAtivo(restricao.produto);
    });

    it('Deveria cadastrar porto para inserção da restrição', function(){
        //Cadastra os portos utilizados no teste
        user.acessaCadPorto();
        user.acessaIncluir();
        console.log('PORTO = '+restricao.porto);
        console.log('PAIS = '+restricao.pais);
        auxPagePorto.inserePorto(restricao.porto, restricao.pais, aux.geraStringRandom(2), aux.geraStringRandom(3));
    });

    it('Deveria cadastrar produto para alterara restrição', function(){
        //Cadastra produto de alteração da restrição
        user.acessaCadProduto();
        user.acessaIncluir();
        auxPageProduto.insereProdutoAtivo(restricao.produtoNovo);
    });

    it('Deveria cadastrar porto para alterar restrição', function(){
        //Cadastra porto de alteração da restrição
        user.acessaCadPorto();
        user.acessaIncluir();
        auxPagePorto.inserePorto(restricao.portoNovo, restricao.paisNovo, aux.geraStringRandom(2), aux.geraStringRandom(3));
    });

    it('Deveria inserir uma restrição', function(){
        //Cadastro de restrição
        user.acessaCadRestricao();
        user.acessaIncluir();
        page.insereRestTodos(restricao.produto, restricao.porto);
        expect(validador.validaSucesso()).toContain('dx-toast-success');
    });

    it('Deveria alterar uma restrição', function(){
        //Altera restrição
        user.acessaCadRestricao();
        page.editaRestricao(restricao.produto, restricao.produtoNovo, restricao.porto, restricao.portoNovo, restricao.pais);
        expect(validador.validaSucesso()).toContain('dx-toast-success');
    });

    it('Deveria excluir uma restrição', function(){
        user.acessaCadRestricao();
        page.excluirRestricao(restricao.produtoNovo, restricao.portoNovo, restricao.paisNovo, 'admincorp', 'kmm2018', 'Aprovado pelo teste automatizado');
        expect(validador.validaSucesso()).toContain('dx-toast-success');
    });

 
});