'use strict';

var ProdutoPage = require('../page_objects/comercial/cadastros/ProdutoPage.js');
var HelpComponent = require('../../helpers/HelpComponent.js');
var Validador = require('../../helpers/ValidaPage.js');
var LoginPage = require('../helpers/LoginPage.js');
var Acessos = require('../helpers/AcessoPage.js');

describe('Regressão CRUD do cadastro de categoria de produto', function(){

    var page = new ProdutoPage();
    var init = new LoginPage();
    var aux = new HelpComponent();
    var validador = new Validador();
    var user = new Acessos();

    var produto = {
        antigo : aux.geraString('Produto'),
        novo : aux.geraString('Novo produto')
    };

    beforeEach(function(){
        browser.driver.manage().window().maximize();
        browser.get('http://o01b18.kmm.com.br:8000/modulos/recinto'); 
    });

    afterEach(function(){
        browser.ignoreSynchronization = false;
    });

    it('Deveria inserir uma categoria de produto', function(){
        init.logar('admincorp', 'kmm2018', true);
        user.acessaCadProduto();
        user.acessaIncluir();
        page.insereProdutoAtivo(produto.antigo);
        expect(validador.validaSucesso()).toContain('dx-toast-success');
    });

    it('Deveria alterar uma categoria de produto', function(){
        user.acessaCadProduto();
        page.editaProduto(produto.antigo, produto.novo);
        expect(validador.validaSucesso()).toContain('dx-toast-success');
    });

    it('Deveria excluir uma categoria de produto', function(){
        user.acessaCadProduto();
        page.excluirProduto(produto.novo, 'admincorp', 'kmm2018', 'Aprovado pelo teste automatizado');
        expect(validador.validaSucesso()).toContain('dx-toast-success');
    });
});