'use strict';

var LoginPage = require('../page_objects/importação/cadastros/LoginPage.js');
var PessoaEstrangeiraPage = require('../page_objects/gestao_cadastros/pessoa/PessoaEstrangeiraPage.js');
var AcessoPage = require('../helpers/AcessoPage.js');

describe('teste', function(){

    var pe = new PessoaEstrangeiraPage();
    var acesso = new AcessoPage();
    var user = new LoginPage();

    it('teste01', function(){

        browser.driver.manage().window().maximize();
        browser.get('http://o01b13.kmm.com.br:8000/modulos/recinto');
        user.logar('kmm_suporte', 'kmm2018', false);
        acesso.acessaCadPessoaEstrangeira();
        acesso.acessaIncluir();

        pe.inserePessoaEstrangeira('1245555','ssp','Jeferson', '04/09/1995',
                              '84040090', 'Rua Padre Nóbrega, 1875', 'casa', 'Oficinas', 'Ponta Grossa',
                              '99888877', '99998888', 'teste@teste.kmm',0);

    });

});