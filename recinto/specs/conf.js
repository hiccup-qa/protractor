let SpecReporter = require('jasmine-spec-reporter').SpecReporter;

exports.config = {
  directConnect: true,
  capabilities: {
    'browserName': 'chrome'
  },
  
  chromeOptions: {
    args: ["--headless", "--disable-gpu", "--window-size=800x600"] 
  },
  
  framework: 'jasmine',
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['spec_regression_restricao.js'],

  jasmineNodeOpts: {
    defaultTimeoutInterval: 90000
  },

  onPrepare: function () {
    global.EC = protractor.ExpectedConditions;

    jasmine.getEnv().addReporter(new SpecReporter({
      displayFailuresSummary: true,
      displayFailuredSpec: true,
      displaySuiteNumber: true,
      displaySpecDuration: true
    }));

  }

};

